import React,{Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import config from './aws-exports';




require('dotenv').config({path:__dirname + '.env'});
const AWS = require('aws-sdk');

const KinesisConfig = {
  accessKeyId: '',
  secretAccessKey: '',
  region:'us-east-1'
}
AWS.config.update(KinesisConfig);


class Sidebar extends Component {
    constructor(props){
        super(props);
    }
     
    saveRecord = (name) => {

      const Kinesis = new AWS.Kinesis();
      const now = new Date();
      
      let menu = {
          id:now.getTime(),
          menu: name,
          type: 'side'
      }
      const params = {
        Data:new Buffer(JSON.stringify(menu)),
        PartitionKey:'1',
        StreamName: config.aws_kinesis_data_stream
      }
      console.log(params);
   
      Kinesis.putRecord(params, (err, data) => {
        console.log(err, data);
      })
  }

    render(){
        return (
            <div className="sidebar">
                <List disablePadding dense>
                {this.props.items.map(({ label, name, ...rest }) => (
                    <ListItem key={name} button {...rest} onClick={() =>{
                        this.saveRecord(name);
                    }}>
                    <ListItemText>{label}</ListItemText>
                    </ListItem>
                ))}
                </List>
            </div>
          )
    }
}



export default Sidebar
import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Amplify, { Analytics } from 'aws-amplify';
import Sidebar from './sidebar';

import awsconfig from './aws-exports';
Amplify.configure(awsconfig);



const items = [
	{ name: 'home', label: 'Home' },
	{ name: 'billing', label: 'Billing' },
	{ name: 'settings', label: 'Settings' },
]

class App extends Component{
	constructor(props){
		super(props);
		this.state = {
			languages : [
				{name: "Php", votes: 0},
				{name: "Python", votes: 0},
				{name: "Go", votes: 0},
				{name: "Java", votes: 0}
			]
		}
	}

	vote  = async (i) =>  {
		let newLanguages = [...this.state.languages];
		newLanguages[i].votes++;
		function swap(array, i, j) {
			var temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
		this.setState({languages: newLanguages});
    	const now = new Date();
    
    	let data = {
			id:now.getTime(),
			menu: newLanguages[i].name,
			type: 'main'
    	}
		console.log(data);
		var resp = await Analytics.record({
			data: data
		});
		console.log(resp);
	}

	render(){
		return(
			<>
				<h1>Vote Your Language!</h1>
				<Sidebar items={items} />
				<div className="languages">
				
					{
						this.state.languages.map((lang, i) => 
							<div key={i} className="language">
								<div className="voteCount">
									{lang.votes}
								</div>
								<div className="languageName">
									{lang.name}
								</div>
								<button onClick={this.vote.bind(this, i)}>Click Here</button>
							</div>
						)
					}
				</div>
			</>
		);
	}
}
export default App;
